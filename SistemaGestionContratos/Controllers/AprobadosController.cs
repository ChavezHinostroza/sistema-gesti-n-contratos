﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Intefaces;
using Modelos;

namespace SistemaGestionContratos.Controllers
{
    public class AprobadosController : Controller
    {
        private InterfaceCriterioEvaluacion repository;
        private InterfacePostulante interfacePostulante;
        private InterfacesPuesto interfacePuesto;

        public AprobadosController(InterfaceCriterioEvaluacion repository, InterfacesPuesto interfacePuesto, InterfacePostulante interfacePostulante)
        {
            this.repository = repository;          
            this.interfacePuesto = interfacePuesto;
            this.interfacePostulante = interfacePostulante;
        }

     
        public ViewResult Index()
        {
            ViewBag.Puesto = interfacePuesto.AllPuesto();
            return View("MostrarAprobados");

        }
        [HttpGet]
        public ActionResult MostrarPostulantes(string id)
        {
            var listPostulante = interfacePostulante.MostrarAprobados(id);
            return PartialView("MostrarPostulantes", listPostulante);
        }

    }
}
