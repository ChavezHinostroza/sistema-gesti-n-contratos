﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


//Importamos
using Intefaces;
using Modelos;
using Validators.PreguntasValidators;


namespace SistemaGestionContratos.Controllers
{
    public class PreguntasController : Controller
    {
        //
        // GET: /Preguntas/
        private IPreguntas repository;
        private InterfacesPuesto interfacePuesto;
        private PreguntasValidators validator;
        private List<Preguntas> preguntasTemp;
        private Random r;

        public PreguntasController(IPreguntas repository, PreguntasValidators validator, InterfacesPuesto interfacePuesto)
        {
            this.repository = repository;
            this.validator = validator;
            this.interfacePuesto = interfacePuesto;
            preguntasTemp = new List<Preguntas>();
            r = new Random(DateTime.Now.Millisecond);
            this.interfacePuesto = interfacePuesto;
        }

        [HttpGet]
        public ActionResult Prueba(int? cantidad, string puesto)
        {
            int[] aleatorios = GenerarAleatorios(cantidad, puesto);
            Preguntas datos;
            for (int i = 0; i < cantidad; i++)
            {
                datos = repository.GenerarPregAleatorias(aleatorios[i], puesto);
                preguntasTemp.Add(datos);
            }
            return View("Prueba", preguntasTemp);
        }

        public int[] GenerarAleatorios(int? cantidad, string puesto)
        {
            int[] aleatorios = new int[50];
            if (cantidad == null)
                cantidad = 0;
            if (puesto == "Ingeniero Sistemas")
            {
                for (int i = 0; i < cantidad; i++)
                {
                    aleatorios[i] = r.Next(1, 10);
                    if (i > 0)    // a partir del segundo numero que genera empezara a comparar que no se repita
                        for (int x = 0; x < 30; x++)  //comprobara que no se repita por 30 veces
                            for (int j = 0; j < i; j++)
                                if (aleatorios[i] == aleatorios[j])
                                    aleatorios[i] = r.Next(1, 10);
                }
            }
            else if (puesto == "Auditor")
            {
                for (int i = 0; i < cantidad; i++)
                {
                    aleatorios[i] = r.Next(11, 20);
                    if (i > 0)    // a partir del segundo numero que genera empezara a comparar que no se repita
                        for (int x = 0; x < 30; x++)  //comprobara que no se repita por 30 veces
                            for (int j = 0; j < i; j++)
                                if (aleatorios[i] == aleatorios[j])
                                    aleatorios[i] = r.Next(11, 20);
                }
            }
            return aleatorios;
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = repository.ByQueryAll(query);
            return View("Inicio", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {

            TraerPreguntas();
            return View("Create");
        }

        private void TraerPreguntas()
        {
            var puesto = interfacePuesto.AllPuesto();
            ViewData["idPuesto"] = new SelectList(puesto, "Id", "Nombre");
            ViewData["idPuesto"] = new SelectList(puesto, "Id", "Nombre");
        }

        [HttpPost]
        public ActionResult Create(Preguntas preguntas)
        {
            if (ModelState.IsValid)
            {
                repository.Store(preguntas);
                TempData["UpdateSuccess"] = "Se Guardó Correctamente";

                return RedirectToAction("Index");
            }
            //if (validator.Pass(preguntas))
            //{

                
            //}
            TraerPreguntas();
            return View("Create", preguntas);
        }

        [HttpGet]
        public ViewResult Edit(int Id)
        {
            var data = repository.Find(Id);
            var puesto = interfacePuesto.AllPuesto();
            ViewData["idPuesto"] = new SelectList(puesto, "Id", "Nombre", data.idPuesto);
            return View("Editar", data);
        }

        [HttpPost]
        public ActionResult Edit(Preguntas preguntas)
        {
            repository.Update(preguntas);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }

    }
}
