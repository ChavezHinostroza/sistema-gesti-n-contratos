﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//Importamos
using Intefaces;
using Modelos;
using Validators.PuestoValidators;

namespace SistemaGestionContratos.Controllers
{
    public class PuestoController : Controller
    {
        //
        // GET: /Postulante/
        private InterfacesPuesto repository;
        private PuestoValidator validator;

        public PuestoController(InterfacesPuesto repository, PuestoValidator validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = repository.ByQueryAll(query);
            return View("ListaPuestos", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View("RegistrarPuesto");
        }

        [HttpPost]
        public ActionResult Create(Puesto puesto)
        {

            if (ModelState.IsValid)
            {
                repository.Store(puesto);

                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }

            return View("RegistrarPuesto", puesto);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.Find(id);
            return View("EditarPuesto", data);
        }

        [HttpPost]
        public ActionResult Edit(Puesto puesto)
        {
            repository.Update(puesto);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }

    }
}
