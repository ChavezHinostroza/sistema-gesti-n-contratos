﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Intefaces;
using Modelos;
using Validators.CriterioEvalValidators;

namespace SistemaGestionContratos.Controllers
{
    public class RubricaCalificacionController : Controller
    {
        private InterfaceCriterioEvaluacion repository;
        private InterfacePostulante interfacePostulante;
        private InterfacesPuesto interfacePuesto;
        private CriterioEvalValidators validator;
        // GET: /RubricaCalificacion/

        public RubricaCalificacionController(InterfaceCriterioEvaluacion repository, CriterioEvalValidators validator, InterfacesPuesto interfacePuesto, InterfacePostulante interfacePostulante)
        {
            this.repository = repository;
            this.validator = validator;
            this.interfacePuesto = interfacePuesto;
            this.interfacePostulante = interfacePostulante;
        }
        public ActionResult Index()
        {
            ViewBag.Puesto = interfacePuesto.AllPuesto();
            return View("Calificacion");
          
        }
        [HttpGet]
        public ActionResult BuscarPostulantes(string id)
        {
            var listPostulante = interfacePostulante.ByQueryAll(id);
            return PartialView("BuscarPostulantes", listPostulante);
        }
    }
}
