﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//Importamos
using Intefaces;
using Modelos;
using Validators.PostulanteValidators;

//Para carga el archivo
using RestSharp;
using RestSharp.Authenticators;
using System.IO;


namespace SistemaGestionContratos.Controllers
{
    public class PostulanteController : Controller
    {
        //
        // GET: /Postulante/
        private InterfacePostulante repository;
        private InterfacesPuesto interfacePuesto;
        private PostulanteValidator validator;
        private string puesto;


        private void TraerPuesto()
        {
            var puesto = interfacePuesto.AllPuesto();
            ViewData["idPuesto"] = new SelectList(puesto, "Id", "Nombre");
        }

        public PostulanteController(InterfacePostulante repository, PostulanteValidator validator, InterfacesPuesto interfacePuesto)
        {
            this.repository = repository;
            this.validator = validator;
            this.interfacePuesto = interfacePuesto;
        }

        [HttpGet]
        public ViewResult Index()
        {
            var datos = repository.All();
            return View("ListaPostulantes", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            TraerPuesto();
            return View("RegistrarPost");
        }


        [HttpPost]
        public ActionResult Create(Postulante postulante, HttpPostedFileBase file)
        {
            validator.Execute(postulante);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));    
            if (ModelState.IsValid)
            {
                postulante.Path = SaveFile(file);
                repository.Store(postulante);

                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }
            TraerPuesto();
            return View("RegistrarPost", postulante);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.Find(id);
            var puesto = interfacePuesto.AllPuesto();
            ViewData["idPuesto"] = new SelectList(puesto, "Id", "Nombre", data.idPuesto);
            return View("EditarPost", data);
        }

        [HttpPost]
        public ActionResult Edit(Postulante postulante)
        {
            repository.Update(postulante);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }

        //[HttpGet]
        //public ActionResult Aprobados(Int32? Id)
        //{
        //    if (Id == null)
        //        Id = 2;
        //    var puestos = interfacePuesto.AllPuesto();
        //    var datos = repository.MostrarAprobados(Id);
        //    ViewData["idPuesto"] = new SelectList(puestos, "Id", "Nombre");
        //    return View("MostrarAprobados", datos);
        //}

        //[HttpPost]
        //public ViewResult Aprobados(String puesto)//Puesto que se ha seleccionado está en la url pero no lo agarra
        //{
        //    var puestos = interfacePuesto.AllPuesto();
        //    this.puesto = puesto;
        //    var datos = repository.MostrarAprobados("Auditor");//Solo falta jalar el puesto que se ha seleccionado
        //    ViewData["idPuesto"] = new SelectList(puestos, "Id", "Nombre");
        //    return View("MostrarAprobados", datos);
        //}

        private String SaveFile(HttpPostedFileBase file)
        {
            string fileName = null;

            if (file != null && file.ContentLength > 0)
            {
                fileName = String.Format("{0:yyyyMMddmmss}", DateTime.Now) + Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Documentos/"), fileName);
                file.SaveAs(path);
            }

            if (fileName != null)

                return "/Documentos/" + fileName;

            return null;

        }

    }
}
