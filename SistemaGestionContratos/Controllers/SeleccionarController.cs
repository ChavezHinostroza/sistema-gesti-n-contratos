﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Intefaces;
using Modelos;

namespace SistemaGestionContratos.Controllers
{
    public class SeleccionarController : Controller
    {
        private InterfaceCriterioEvaluacion repository;
        private InterfacePostulante interfacePostulante;
        private InterfacesPuesto interfacePuesto;
        
        // GET: /RubricaCalificacion/

        public SeleccionarController(InterfaceCriterioEvaluacion repository, InterfacesPuesto interfacePuesto, InterfacePostulante interfacePostulante)
        {
            this.repository = repository;          
            this.interfacePuesto = interfacePuesto;
            this.interfacePostulante = interfacePostulante;
        }
        [HttpGet]
        public ViewResult Index()
        {
            ViewBag.Puesto = interfacePuesto.AllPuesto();
            return View("SeleccionarAprobados");

        }
        [HttpGet]
        public ActionResult SeleccionarPostulantes(string id)
        {
            var listPostulante = interfacePostulante.ByQueryAll(id);
            return PartialView("SeleccionarPostulantes", listPostulante);
        }

    }
}
