﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//Importamos
using Intefaces;
using Modelos;
using Validators.CriterioEvalValidators;

namespace SistemaGestionContratos.Controllers
{
    public class CriteriosEvaluacionController : Controller
    {
         // GET: /Postulante/
        private InterfaceCriterioEvaluacion repository;
        private CriterioEvalValidators validator;

        public CriteriosEvaluacionController(InterfaceCriterioEvaluacion repository, CriterioEvalValidators validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        [HttpGet]
        public ViewResult Index()
        //public ViewResult Index(string query = "")
        {
            var datos = repository.AllCriteriosEvaluacion();
            return View("ListaCriteriosEval", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View("RegistrarCriterioEval");
        }

        [HttpPost]
        public ActionResult Create(CriteriosEvaluacion criterioeval)
        {

            if (ModelState.IsValid)
            {
                repository.Store(criterioeval);

                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }

            return View("RegistrarCriterioEval", criterioeval);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.Find(id);
            return View("EditarCriterioEval", data);
        }

        [HttpPost]
        public ActionResult Edit(CriteriosEvaluacion criterioeval)
        {
            repository.Update(criterioeval);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }

       

    }
}
