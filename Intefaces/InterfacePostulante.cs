﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Intefaces
{
  public  interface InterfacePostulante
    {
        List<Postulante> All();
        void Store(Postulante postulante);
        Postulante Find(int id);
        void Update(Postulante producto);
        void Delete(int id);
        List<Postulante> ByQueryAll(string query);
        //List<Postulante> ByQueryAll(Int32? Id);
        int GetDNISame(string dni);
        List<Postulante> MostrarAprobados(string puesto);

    }
}
