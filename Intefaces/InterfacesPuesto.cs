﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;


namespace Intefaces
{
  public  interface InterfacesPuesto
    {
        List<Puesto> AllPuesto();
        void Store(Puesto puesto);
        Puesto Find(int id);
        void Update(Puesto puesto);
        void Delete(int id);
        List<Puesto> ByQueryAll(string query);
       
    }
}
