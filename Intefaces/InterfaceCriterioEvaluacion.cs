﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Intefaces
{
   public interface InterfaceCriterioEvaluacion
    {
       List<CriteriosEvaluacion> AllCriteriosEvaluacion();
       void Store(CriteriosEvaluacion criterioevaluacion);
       CriteriosEvaluacion Find(int id);
        void Update(CriteriosEvaluacion criterioevaluacion);
        void Delete(int id);
        List<CriteriosEvaluacion> ByQueryAll(string query);
    }
}
