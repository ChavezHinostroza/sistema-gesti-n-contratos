﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Modelos
{
  public  class Preguntas
    {
        public int Id { get; set; }

        [Required]
        public string Pregunta { get; set; }
        //public String Puesto { get; set; }

        public Puesto Puesto { get; set; }
        public Int32 idPuesto { get; set; }
    }
}
