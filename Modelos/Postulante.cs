﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
  public  class Postulante
    {
      //www.rejex101.com -> Expresiones Regulares
        public int Id { get; set; }
        [Required]
        public string Nombres { get; set; }
        [Required]
        public string ApellidoPaterno { get; set; }
       [Required]
        public string ApellidoMaterno { get; set; }
        [Required]
        public string Sexo { get; set; }
       [Required]
        //[Range(8, 10, ErrorMessage = "Ingrese un número celular correcto")]
        public string Celular { get; set; }
       [Required]
       [EmailAddress(ErrorMessage = "El email no tiene el formato correcto")]
        public string CorreoElectronico { get; set; }
       [Required]
        //[RegularExpression("^({8})$([0-9]|-)*", ErrorMessage = "DNI incorrecto")]
        public string DNI { get; set; }

        public bool EstaAprobado { get; set; }
        public string Path { get; set; }


      // Nuevo      

         public Puesto Puesto { get; set; }
         public Int32 idPuesto { get; set; }
    }
}
