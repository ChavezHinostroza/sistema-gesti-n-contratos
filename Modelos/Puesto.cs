﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Validar
using System.ComponentModel.DataAnnotations;

namespace Modelos
{
   public class Puesto
    {
        public Int32 Id { get; set; }
        [Required]
        [RegularExpression("[A-Za-z ]*", ErrorMessage = "Ingrese solo letras")]
        public string Nombre { get; set; }
        [Required]
        [RegularExpression("[0-9 ]*", ErrorMessage = "Ingrese solo números")]
        public int CantidadVacantes { get; set; }
        [Required]
        [RegularExpression("[A-Za-z ]*", ErrorMessage = "Ingrese solo letras")]
        public  string Descripcion { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }

    }   
   
}
