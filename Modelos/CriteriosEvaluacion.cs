﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
 public  class CriteriosEvaluacion
    {
        public Int32 Id { get; set; }
        public int Presencia { get; set; }
        public int DominioTema { get; set; }
        public string Personalidad  { get; set; }
        public Int32 Puntaje { get; set; }

        public Int32 IdPuestokf { get; set; }
        public Puesto Puesto { get; set; }

        public Int32 IdPostulantekf { get; set; }
        public Postulante Postulante { get; set; }

    }
}
