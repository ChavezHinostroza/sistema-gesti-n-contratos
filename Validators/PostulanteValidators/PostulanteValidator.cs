﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Importamos
using Modelos;
using Repositorios.Repos;
using Repository;


namespace Validators.PostulanteValidators
{
    public class PostulanteValidator
    {
        public Dictionary<String, String> errors = new Dictionary<string, string>();
        public void Execute(Postulante postulante)
        {
            Pass(postulante);
        }

        public virtual bool Pass(Postulante postulante)
        {
            if (!DniNoExiste(postulante.DNI))

                errors.Add("DNI", "Este usuario ya existe");

            return true;
        }


        private bool DniNoExiste(string dni)
        {
            var repo = new PostulanteRepository(new ContratosContext());
            if (repo.GetDNISame(dni) > 0)
                return false;
            return true;
        }

    }
}
