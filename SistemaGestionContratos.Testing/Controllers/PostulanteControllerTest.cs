﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Importamos
using NUnit.Framework;
using Validators.PostulanteValidators;
using Modelos;
using Intefaces;
using Moq;
using SistemaGestionContratos.Controllers;
using System.Web.Mvc;

//Para carga el archivo
using RestSharp;
using RestSharp.Authenticators;
using System.IO;

namespace SistemaGestionContratos.Testing.Controllers
{
    [TestFixture]
    public class PostulanteControllerTest
    {
        [Test]
        public void TestCreateReturnViewRegistrarPostulante()
        {
            var mockPuesto = new Mock<InterfacesPuesto>();
            mockPuesto.Setup(o => o.AllPuesto()).Returns(new List<Puesto>());

            var controller = new PostulanteController(null, null, mockPuesto.Object);
            var view = controller.Create();
            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("RegistrarPost", view.ViewName);
        }

        [Test]
        public void TestIndexCallAllMethodFromRepository()
        {
            var mock = new Mock<InterfacesPuesto>();
            mock.Setup(o => o.ByQueryAll("")).Returns(new List<Puesto>());

            var controller = new PuestoController(mock.Object, null);

            controller.Index();

            mock.Verify(o => o.ByQueryAll(""), Times.Exactly(1));
        }

       

    }
}
