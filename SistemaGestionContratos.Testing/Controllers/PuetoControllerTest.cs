﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using NUnit.Framework;
using Validators.PuestoValidators;
using Modelos;
using Intefaces;
using Moq;
using System.Web.Mvc;
using SistemaGestionContratos.Controllers;

namespace SistemaGestionContratos.Testing.Controllers
{
  public  class PuetoControllerTest
    {
        [Test]
        public void TestIndexReturnViewPuesto()
        {
            var mock = new Mock<InterfacesPuesto>();

            var query = "";
            

            mock.Setup(o => o.ByQueryAll(query)).Returns(new List<Puesto>());

            var controller = new PuestoController(mock.Object, null);

            var view = controller.Index(query);

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("ListaPuestos", view.ViewName);
            Assert.IsInstanceOf(typeof(List<Puesto>), view.Model);

        }

        [Test]
        public void TestIndexCallAllMethodFromRepository()
        {
            var mock = new Mock<InterfacesPuesto>();
            mock.Setup(o => o.ByQueryAll("")).Returns(new List<Puesto>());

            var controller = new PuestoController(mock.Object, null);

            controller.Index();

            mock.Verify(o => o.ByQueryAll(""), Times.Exactly(1));
        }

        [Test]
        public void TestCreateReturnView()
        {
            var controller = new PuestoController(null, null);

            var view = controller.Create();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("RegistrarPuesto", view.ViewName);
        }

        //[Test]
        //public void TestPostCreateOKReturnRedirect()
        //{
        //    var producto = new Puesto();

        //    var repositoryMock = new Mock<InterfacesPuesto>();

        //    repositoryMock.Setup(o => o.Store(new Puesto()));

        //    var validatorMock = new Mock<PuestoValidator>();

        //    validatorMock.Setup(o => o.Pass(producto)).Returns(true);

        //    var controller = new PuestoController(repositoryMock.Object, validatorMock.Object);

        //    var redirect = (RedirectToRouteResult)controller.Create(producto);

        //    Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        //}

        //[Test]
        //public void TestPostCreateCallStoreMethodFromRepository()
        //{
        //    var puesto = new Puesto();

        //    var repositoryMock = new Mock<InterfacesPuesto>();

        //    var validatorMock = new Mock<PuestoValidator>();
            
        //    validatorMock.Setup(o => o.Pass(puesto)).Returns(true);

        //    repositoryMock.Setup(o => o.Store(puesto));

        //    var controller = new PuestoValidator(repositoryMock.Object);

        //    var redirect = controller.Create(puesto);

        //    repositoryMock.Verify(o => o.Store(puesto), Times.Once());

        //}

        //[Test]

        //public void TestPostCreateReturnViewWithErrorsWhenValidationFail()
        //{
        //    var puesto = new Puesto { };

        //    var mock = new Mock<PuestoController>();

        //    mock.Setup(o => o.Pass(puesto)).Returns(false);

        //    var controller = new PuestoController(null, mock.Object);

        //    var view = controller.Create(puesto);

        //    Assert.IsInstanceOf(typeof(ViewResult), view);
        //}
    }
}
