﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;
using Validators.PostulanteValidators;
using Modelos;
using Intefaces;
using Moq;
using SistemaGestionContratos.Controllers;
using System.Web.Mvc;

namespace SistemaGestionContratos.Testing.Controllers
{
    [TestFixture]
    class AprobadosControllerTest
    {
        [Test]
        public void TestReturnViewMostrarAprobados()
        {
            var mockPuesto = new Mock<InterfacesPuesto>();
            mockPuesto.Setup(o => o.AllPuesto()).Returns(new List<Puesto>());

            var mockPostulante = new Mock<InterfacePostulante>();
            mockPostulante.Setup(o => o.ByQueryAll("")).Returns(new List<Postulante>());

            var controller = new AprobadosController(null, mockPuesto.Object, mockPostulante.Object);
            var view = controller.Index();
            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("MostrarAprobados", view.ViewName);
        }
        [Test]
        public void TestCallMethodFromaprobadosController()
        {
            var mock = new Mock<InterfacesPuesto>();
            mock.Setup(o => o.ByQueryAll("")).Returns(new List<Puesto>());

            var mockPost = new Mock<InterfacePostulante>();
            mockPost.Setup(o => o.ByQueryAll("")).Returns(new List<Postulante>());

            var controller = new SeleccionarController(null, mock.Object, mockPost.Object);

            controller.SeleccionarPostulantes("");

            mockPost.Verify(o => o.ByQueryAll(""), Times.Exactly(1));
        }
    }
}
