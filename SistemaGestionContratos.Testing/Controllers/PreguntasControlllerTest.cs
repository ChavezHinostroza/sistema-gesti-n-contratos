﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionContratos.Controllers;

using NUnit.Framework;
using Validators.PreguntasValidators;
using Modelos;
using Intefaces;
using Moq;
using System.Web.Mvc;


namespace SistemaGestionContratos.Testing.Controllers
{
    [TestFixture]
   public class PreguntasControlllerTest
    {
        [Test]
        public void TestCreateReturnViewRegistrarPregunta()
        {
            var mockPuesto = new Mock<InterfacesPuesto>();
            mockPuesto.Setup(o => o.AllPuesto()).Returns(new List<Puesto>());

            var controller = new PreguntasController(null, null, mockPuesto.Object);

            var view = controller.Create();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Create", view.ViewName);
        }

        //[Test]
      
        //public void TestCreateReturnViewEditarPregunta()
        //{
        //    var mockPuesto = new Mock<InterfacesPuesto>();
        //    var mockPregunta = new Mock<IPreguntas>();
        //    mockPuesto.Setup(o => o.AllPuesto()).Returns(new List<Puesto>());
            
        //    mockPregunta.Setup(o => o.All()).Returns(new List<Preguntas>());


        //    var controller = new PreguntasController(mockPregunta.Object, null, mockPuesto.Object);            

        //    var view = controller.Edit(1);

        //    Assert.IsInstanceOf(typeof(ViewResult), view);
        //    Assert.AreEqual("Editar", view.ViewName);
        //}

        [Test]
        public void TestIndexCallAllMethodFromRepository()
        {
            var mockPreg = new Mock<InterfacesPuesto>();
            mockPreg.Setup(o => o.ByQueryAll("")).Returns(new List<Puesto>());

            var controller = new PuestoController(mockPreg.Object, null);

            controller.Index();

            mockPreg.Verify(o => o.ByQueryAll(""), Times.Exactly(1));
        }

    }
}
