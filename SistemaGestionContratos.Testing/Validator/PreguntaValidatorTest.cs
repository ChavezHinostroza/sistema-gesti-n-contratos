﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Validators.PreguntasValidators;
using Modelos;
using NUnit.Framework;

namespace SistemaGestionContratos.Testing.Validator
{
    [TestFixture]
   public class PreguntaValidatorTest
    {
        [Test]
        public void PreguntasSinNombreDebeRetornarFalse()
        {
            var validator = new PreguntasValidators();
            var pregunta = new Preguntas();

            var result = validator.Pass(pregunta);

            Assert.False(result);
        }

        [Test]
        public void PreguntaConNombreYCodigoDebeRetornarTrue()
        {
            var validator = new PreguntasValidators();
            var pregunta = new Preguntas
            {
                Id = 1,
                Pregunta = "pregunta 1"
               
            };

            var result = validator.Pass(pregunta);

            Assert.True(result);

        }
    }
}
