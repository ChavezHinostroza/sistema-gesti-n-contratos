﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Repository;
using Moq;
using Modelos;
using Repositorios.Repos;
using NUnit.Framework;

namespace SistemaGestionContratos.Testing.Repository
{
    [TestFixture]
   public class PreguntaRepositoryTest
    {
        [Test]
        public void AllReturnListPreguntas()
        {
            var repo = new PreguntasRepository(new ContratosContext());

            var result = repo.All();

            Assert.IsInstanceOf(typeof(List<Preguntas>), result);
            Assert.AreEqual(0, result.Count());

        }

        //[Test]
        //public void AllReturnFindPregunta()
        //{
        //    var data = new List<Preguntas>
        //    {
        //        new Preguntas { Id = 1,Pregunta= "nuevapregunta" },
        //        new Preguntas { Id = 2, Pregunta= "Pregunta" }
        //    }.AsQueryable();

        //    var dbMock = new Mock<IDbSet<Preguntas>>();

        //    dbMock.Setup(o => o.Provider).Returns(data.Provider);
        //    dbMock.Setup(o => o.Expression).Returns(data.Expression);
        //    dbMock.Setup(o => o.ElementType).Returns(data.ElementType);
        //    dbMock.Setup(o => o.GetEnumerator()).Returns(data.GetEnumerator());

        //    var dbContextMock = new Mock<ContratosContext>();
        //    dbContextMock.Setup(x => x.Preguntas)
        //        .Returns(dbMock.Object);

        //    var repo = new PreguntasRepository(dbContextMock.Object);

        //    var result = repo.Find(1);


        //    Assert.IsInstanceOf(typeof(Preguntas), result);
        //    Assert.AreEqual(1, result.Id);
        //    Assert.AreEqual("nuevapregunta", result.Pregunta);
        //    Assert.AreEqual("1", result.Id);

        //}

        //private void SetUpDbSetMock<T>(IQueryable<T> data, Mock<IDbSet<T>> dbMock) where T : class
        //{
        //    dbMock.Setup(o => o.Provider).Returns(data.Provider);
        //    dbMock.Setup(o => o.Expression).Returns(data.Expression);
        //    dbMock.Setup(o => o.ElementType).Returns(data.ElementType);
        //    dbMock.Setup(o => o.GetEnumerator()).Returns(data.GetEnumerator());
        //}


        //[Test]
        //public void ByQueryAllTestIsOk()
        //{
        //    var data = new List<Producto>
        //    {
        //        new Producto { Id = 1, Code = "001", Name = "Galleta" },
        //        new Producto { Id = 2, Code = "002", Name = "Producto 2" },
        //        new Producto { Id = 3, Code = "003", Name = "Pepsi" },
        //        new Producto { Id = 4, Code = "004", Name = "Perú" },
        //        new Producto { Id = 5, Code = "005", Name = "Petróleo" }
        //    }.AsQueryable();

        //    var dbMock = new Mock<IDbSet<Producto>>();

        //    dbMock.Setup(o => o.Provider).Returns(data.Provider);
        //    dbMock.Setup(o => o.Expression).Returns(data.Expression);
        //    dbMock.Setup(o => o.ElementType).Returns(data.ElementType);
        //    dbMock.Setup(o => o.GetEnumerator()).Returns(data.GetEnumerator());

        //    var dbContextMock = new Mock<MvcContext>();
        //    dbContextMock.Setup(x => x.Productos)
        //        .Returns(dbMock.Object);

        //    var repo = new ProductoRepository(dbContextMock.Object);

        //    var result = repo.ByQueryAll("Pe", null);

        //    Assert.AreEqual(3, result.Count());


        //}

    }
}
