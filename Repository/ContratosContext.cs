﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Importamos
using Repository.Mapping;
using System.Data.Entity;
using Modelos;


namespace Repository
{
 public   class ContratosContext:DbContext
    {
     public ContratosContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ContratosContext>());
        }
        public DbSet<Postulante> Postulantes { get; set; }
        public DbSet<Preguntas> Preguntas { get; set; }
        public DbSet<Puesto> Puestos { get; set; }
        public DbSet<CriteriosEvaluacion> CriteriosEvaluaciones { get; set; }
      

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PostulanteMap());
            modelBuilder.Configurations.Add(new PreguntasMap());
            modelBuilder.Configurations.Add(new PuestoMap());
            modelBuilder.Configurations.Add(new CriteriosEvaluacionMap());
        }
    }
}
