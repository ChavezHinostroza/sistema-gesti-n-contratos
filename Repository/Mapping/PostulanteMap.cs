﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Importamos
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Modelos;


namespace Repository.Mapping
{
    public class PostulanteMap : EntityTypeConfiguration<Postulante>
    {
        public PostulanteMap()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.ApellidoMaterno).IsRequired().HasMaxLength(50);
            this.Property(p => p.Nombres).IsRequired().HasMaxLength(50);
            this.Property(p => p.DNI).IsRequired();
            this.Property(p => p.ApellidoPaterno).IsRequired().HasMaxLength(50);
            this.Property(p => p.Celular).IsRequired();
            this.Property(p => p.CorreoElectronico).IsRequired().HasMaxLength(50);
            this.Property(p => p.Sexo).IsRequired();

            
            this.Property(p => p.EstaAprobado).IsOptional();

            this.HasRequired(p => p.Puesto).WithMany().HasForeignKey(p => p.idPuesto).WillCascadeOnDelete(true); //Relaciones

            this.ToTable("Postulante");
        }
    }
}
