﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Importamos
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Modelos;


namespace Repository.Mapping
{
    public   class CriteriosEvaluacionMap:EntityTypeConfiguration<CriteriosEvaluacion>
    {
         public CriteriosEvaluacionMap()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.Presencia).IsRequired();
            this.Property(p => p.DominioTema).IsRequired();
            this.Property(p => p.Personalidad).IsRequired();
            this.Property(p => p.Puntaje).IsOptional();


            this.HasRequired(p => p.Puesto).WithMany().HasForeignKey(p => p.IdPuestokf).WillCascadeOnDelete(false);
            this.HasRequired(p => p.Postulante).WithMany().HasForeignKey(p => p.IdPostulantekf).WillCascadeOnDelete(false);

            this.ToTable("CriteriosEvaluacion");
        }
    }
}
