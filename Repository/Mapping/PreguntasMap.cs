﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Importamos
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Modelos;


namespace Repository.Mapping
{
    public class PreguntasMap : EntityTypeConfiguration<Preguntas>
    {
        public PreguntasMap()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.Pregunta).IsOptional().HasMaxLength(250);

            this.HasRequired(p => p.Puesto).WithMany().HasForeignKey(p => p.idPuesto).WillCascadeOnDelete(true); //Relaciones

            this.ToTable("Preguntas");
        }
    }
}
