﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Modelos;

namespace Repository.Mapping
{
   public class PuestoMap:EntityTypeConfiguration<Puesto>
    {
       public PuestoMap()
       {
           this.HasKey(p => p.Id);
           this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.Property(p => p.Nombre).IsRequired().HasMaxLength(100);
           this.Property(p => p.Descripcion).IsRequired().HasMaxLength(250);
           this.Property(p => p.CantidadVacantes).IsOptional();
           this.Property(p => p.FechaInicio).IsOptional();
           this.Property(p => p.FechaFin).IsOptional();

          
           this.ToTable("Puesto");
       }
    }
}
