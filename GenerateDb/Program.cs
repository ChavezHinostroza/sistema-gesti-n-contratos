﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;
using Repository;

namespace GenerateDb
{
    class Program
    {
        //string date = "21/08/2015";
        //DateTime fechaAperCaja = Convert.ToDateTime(date);

        static void Main(string[] args)
        {
            var puesto01 = new Puesto()
            {
                Nombre = "Ingeniero Sistemas",
                Descripcion = "Area Informatica",
                CantidadVacantes = 2
            };
            var puesto02 = new Puesto()
            {
                Nombre = "Auditor",
                Descripcion = "Auditoria Externa",
                CantidadVacantes = 1
            };

            var _context = new ContratosContext();
            Console.WriteLine("Creando Base De Datos");
            _context.Puestos.Add(puesto01);
            _context.Puestos.Add(puesto02);
            _context.SaveChanges();

            var Postulante01 = new Postulante()
            {
                Nombres = "Carlos",
                ApellidoPaterno = "Chavrz",
                ApellidoMaterno = "Hinostroza",
                Sexo = "M",
                Celular = "997634625",
                CorreoElectronico = "carlo@hotmail.com",
                DNI = "78451976",
                Path = "/Documentos/CV-UNO.txt",
                EstaAprobado = true,
                idPuesto = puesto01.Id
            };
            var Postulante02 = new Postulante()
            {
                Nombres = "Tony",
                ApellidoPaterno = "Torres",
                ApellidoMaterno = "Vargas",
                Sexo = "M",
                Celular = "978451232",
                CorreoElectronico = "Hijomipona@jajaj.com",
                DNI = "78412563",
                Path = "/Documentos/CV-DOS.txt",
                EstaAprobado = false,
                idPuesto = puesto01.Id
            };
            var Postulante03 = new Postulante()
            {
                Nombres = "Yisus",
                ApellidoPaterno = "Chegne",
                ApellidoMaterno = "Chavez",
                Sexo = "M",
                Celular = "976451823",
                CorreoElectronico = "chegene@yahhoo.es",
                DNI = "12457896",
                Path = "/Documentos/CV-TRES.txt",
                EstaAprobado = true,
                idPuesto = puesto01.Id
            };
            var Postulante04 = new Postulante()
            {
                Nombres = "Ginnie",
                ApellidoPaterno = "Vigo",
                ApellidoMaterno = "Torres",
                Sexo = "F",
                Celular = "97654832",
                CorreoElectronico = "ginnie@yahhoo.es",
                DNI = "13243546",
                Path = "/Documentos/CV-CUATRO.txt",
                EstaAprobado = false,
                idPuesto = puesto02.Id
            };
            var Postulante05 = new Postulante()
            {
                Nombres = "Emilya",
                ApellidoPaterno = "Cabos",
                ApellidoMaterno = "Chavez",
                Sexo = "F",
                Celular = "768788980",
                CorreoElectronico = "emilyaxD@yahhoo.es",
                DNI = "12457896",
                Path = "/Documentos/CV-CINCO.txt",
                EstaAprobado = false,
                idPuesto = puesto02.Id
            };
            var Postulante06 = new Postulante()
            {
                Nombres = "Laura",
                ApellidoPaterno = "Bazan",
                ApellidoMaterno = "Flores",
                Sexo = "F",
                Celular = "976488890",
                CorreoElectronico = "lauOpiu@yahhoo.es",
                DNI = "15678576",
                Path = "/Documentos/CV-SEIS.txt",
                EstaAprobado = true,
                idPuesto = puesto02.Id
            };
            var pregunta01 = new Preguntas()
            {
                Pregunta = "¿Qué hacer en caso de perder información?",
                Puesto = puesto01,
                idPuesto = puesto01.Id
            };
            var pregunta02 = new Preguntas()
            {
                Pregunta = "¿Cómo crear una base de datos?",
                Puesto = puesto01,
                idPuesto = puesto01.Id
            };
            var pregunta03 = new Preguntas()
            {
                Pregunta = "¿Qué es una arquitectura en capas?",
                Puesto = puesto01,
                idPuesto = puesto01.Id
            };
            var pregunta04 = new Preguntas()
            {
                Pregunta = "¿Qué culpa tiene Fatmagul?",
                Puesto = puesto01,
                idPuesto = puesto01.Id
            };
            var pregunta05 = new Preguntas()
            {
                Pregunta = "¿Qué es un Moq y cómo se usa?",
                Puesto = puesto01,
                idPuesto = puesto01.Id
            };
            var pregunta06 = new Preguntas()
            {
                Pregunta = "Una razón para crear un procedimiento almacenado es: ",
                Puesto = puesto01,
                idPuesto = puesto01.Id
            };
            var pregunta07 = new Preguntas()
            {
                Pregunta = "¿Cuales son los lenguajes que más domina?",
                Puesto = puesto01,
                idPuesto = puesto01.Id
            };
            var pregunta08 = new Preguntas()
            {
                Pregunta = "Si dos y dos son cuatro, cuatro y dos son seis, ¿Cuánto es seis y dos?",
                Puesto = puesto01,
                idPuesto = puesto01.Id
            };
            var pregunta09 = new Preguntas()
            {
                Pregunta = "Programe una calculadora. Utilice el lenguaje que desee",
                Puesto = puesto01,
                idPuesto = puesto01.Id
            };
            var pregunta10 = new Preguntas()
            {
                Pregunta = "Usted ejecuta la instrucción: INCERT INTO Carretera VALUES (1234, 36) Cual es el resultado? ",
                Puesto = puesto01,
                idPuesto = puesto01.Id
            };
            var pregunta11 = new Preguntas()
            {
                Pregunta = "¿Cómo trabaja code First?",
                Puesto = puesto02,
                idPuesto = puesto02.Id
            };
            var pregunta12 = new Preguntas()
            {
                Pregunta = "¿La arquitectura de capas puede tener varias capas?",
                Puesto = puesto02,
                idPuesto = puesto02.Id
            };
            var pregunta13 = new Preguntas()
            {
                Pregunta = "Mostrar los ids de las categorías de productos utilizando solo la información de la tabla producto.",
                Puesto = puesto02,
                idPuesto = puesto02.Id
            };
            var pregunta14 = new Preguntas()
            {
                Pregunta = "Crear una consulta para mostrar el nombre de producto y stock para las productos que tienen un stock mayor que 50",
                Puesto = puesto02,
                idPuesto = puesto02.Id
            };
            var pregunta15 = new Preguntas()
            {
                Pregunta = "Crear una consulta para mostrar el  apellido paterno y nombres para  la persona con R.U.C igual a -10266115645-",
                Puesto = puesto02,
                idPuesto = puesto02.Id
            };
            var pregunta16 = new Preguntas()
            {
                Pregunta = "Mostrar todos los datos de las ventas que se hicieron en el mes mayo ordenados por fecha de venta en orden ascendente.",
                Puesto = puesto02,
                idPuesto = puesto02.Id
            };
            var pregunta17 = new Preguntas()
            {
                Pregunta = "Actualizar la información de la tabla persona de modo que todos los empleados que tienen como apellido -Flores- estén ubicados en Lima",
                Puesto = puesto02,
                idPuesto = puesto02.Id
            };
            var pregunta18 = new Preguntas()
            {
                Pregunta = "Escribir una sentencia SQL que elimine los productos con stock igual a 100 de la categoría -Acabados-",
                Puesto = puesto02,
                idPuesto = puesto02.Id
            };
            var pregunta19 = new Preguntas()
            {
                Pregunta = "Modifique el stock de los productos que empiezan con la palabra ‘Adaptador’ a un valor de 50.",
                Puesto = puesto02,
                idPuesto = puesto02.Id
            };
            var pregunta20 = new Preguntas()
            {
                Pregunta = "Escribir una sentencia SQL que aumente el precio de los productos con id igual a ‘001’ en un 20%",
                Puesto = puesto02,
                idPuesto = puesto02.Id
            };
           
            _context.Postulantes.Add(Postulante01);
            _context.Postulantes.Add(Postulante02);
            _context.Postulantes.Add(Postulante03);
            _context.Postulantes.Add(Postulante04);
            _context.Postulantes.Add(Postulante05);
            _context.Postulantes.Add(Postulante06);
            _context.Preguntas.Add(pregunta01);
            _context.Preguntas.Add(pregunta02);
            _context.Preguntas.Add(pregunta03);
            _context.Preguntas.Add(pregunta04);
            _context.Preguntas.Add(pregunta05);
            _context.Preguntas.Add(pregunta06);
            _context.Preguntas.Add(pregunta07);
            _context.Preguntas.Add(pregunta08);
            _context.Preguntas.Add(pregunta09);
            _context.Preguntas.Add(pregunta10);
            _context.Preguntas.Add(pregunta11);
            _context.Preguntas.Add(pregunta12);
            _context.Preguntas.Add(pregunta13);
            _context.Preguntas.Add(pregunta14);
            _context.Preguntas.Add(pregunta15);
            _context.Preguntas.Add(pregunta16);
            _context.Preguntas.Add(pregunta17);
            _context.Preguntas.Add(pregunta18);
            _context.Preguntas.Add(pregunta19);
            _context.Preguntas.Add(pregunta20);

            _context.SaveChanges();

            Console.WriteLine("Base de datos creada con exito!!");
            Console.ReadLine();
        }
    }
}
