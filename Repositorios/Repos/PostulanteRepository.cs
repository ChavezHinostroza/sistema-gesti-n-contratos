﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Imporamos
using Repository;
using Intefaces;
using Modelos;
using System.Data;


namespace Repositorios.Repos
{
  public  class PostulanteRepository: InterfacePostulante
    {
        ContratosContext entities;
        public PostulanteRepository(ContratosContext entities)
        {
            this.entities = entities;
        }

        public List<Postulante> All()
        {
            var result = from p in entities.Postulantes.Include("Puesto") select p;
            return result.ToList();
        }


        public void Store(Postulante postulante)
        {
            entities.Postulantes.Add(postulante);
            entities.SaveChanges();
        }


        public Postulante Find(int id)
        {
            var result = from p in entities.Postulantes.Include("Puesto") where p.Id == id select p;
            return result.FirstOrDefault();
        }

        public void Update(Postulante postulante)
        {
            var result = (from p in entities.Postulantes where p.Id == postulante.Id select p).First();

            result.Nombres = postulante.Nombres;
            result.ApellidoPaterno = postulante.ApellidoPaterno;
            result.ApellidoMaterno = postulante.ApellidoMaterno;
            result.Sexo = postulante.Sexo;
            result.Celular = postulante.Celular;
            result.CorreoElectronico = postulante.CorreoElectronico;
            result.DNI = postulante.DNI;
            result.idPuesto=postulante.idPuesto;

            entities.SaveChanges();
        }

        public void Delete(int id)
        {
            var result = (from p in entities.Postulantes where p.Id == id select p).First();
            entities.Postulantes.Remove(result);
            entities.SaveChanges();
        }

        //public List<Postulante> ByQueryAll(Int32? Id)
        //{
        //    var dbQuery = (from p in entities.Postulantes.Include("Puesto") where p.Puesto.Id == Id select p);
        //    return dbQuery.ToList();
        //}

        public List<Postulante> MostrarAprobados(string puesto)
        {
            var dbQuery = (from p in entities.Postulantes.Include("Puesto") where p.EstaAprobado == true && p.Puesto.Nombre == puesto select p);
            return dbQuery.ToList();
        }


        public int GetDNISame(string dni)
        {
            return (from p in entities.Postulantes where p.DNI == dni select p).Count();
        }


        public List<Postulante> ByQueryAll(string query)
        {
            var dbQuery = (from p in entities.Postulantes.Include("Puesto") where p.Puesto.Nombre == query select p);
            return dbQuery.ToList();
        }
    }
}
