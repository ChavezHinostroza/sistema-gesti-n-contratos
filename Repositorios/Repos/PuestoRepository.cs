﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Imporamos
using Repository;
using Intefaces;
using Modelos;
using System.Data;

namespace Repositorios.Repos
{
  public  class PuestoRepository:InterfacesPuesto
    {
        ContratosContext entities;
        public PuestoRepository(ContratosContext entities)
        {
            this.entities = entities;
        }

        public void Store(Puesto puesto)
        {
            entities.Puestos.Add(puesto);
            entities.SaveChanges();
        }

        public Puesto Find(int id)
        {
            var result = from p in entities.Puestos where p.Id == id select p;
            return result.FirstOrDefault();
        }

        public void Update(Puesto puesto)
        {
            var result = (from p in entities.Puestos where p.Id == puesto.Id select p).First();

            result.Nombre = puesto.Nombre;
            result.CantidadVacantes = puesto.CantidadVacantes;
            result.Descripcion = puesto.Descripcion;
            result.FechaInicio= puesto.FechaInicio;
            result.FechaFin = puesto.FechaFin;
            entities.SaveChanges();
        }

        public void Delete(int id)
        {
            var result = (from p in entities.Puestos where p.Id == id select p).First();
            entities.Puestos.Remove(result);
            entities.SaveChanges();
        }

        public List<Puesto> ByQueryAll(string query)
        {
            var dbQuery = (from p in entities.Puestos select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.Nombre.Contains(query));

            return dbQuery.ToList();
        }

        public List<Puesto> AllPuesto()
        {
            var result = from p in entities.Puestos select p;
            return result.ToList();
        }
    }
}
