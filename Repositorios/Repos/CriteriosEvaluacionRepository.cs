﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Importamos
using Repository;
using Intefaces;
using Modelos;
using System.Data;

namespace Repositorios.Repos
{
  public  class CriteriosEvaluacionRepository:InterfaceCriterioEvaluacion
    {
        ContratosContext entities;
        public CriteriosEvaluacionRepository(ContratosContext entities)
        {
            this.entities = entities;
        }

        public List<CriteriosEvaluacion> AllCriteriosEvaluacion()
        {
            var result = from p in entities.CriteriosEvaluaciones select p;
            return result.ToList();
        }

        public void Store(CriteriosEvaluacion criterioevaluacion)
        {
            entities.CriteriosEvaluaciones.Add(criterioevaluacion);
            entities.SaveChanges();
        }

        public CriteriosEvaluacion Find(int id)
        {
            var result = from p in entities.CriteriosEvaluaciones.Include("Puesto").Include("Postulante") where p.Id == id select p;
            return result.FirstOrDefault();
        }

        public void Update(CriteriosEvaluacion criterioevaluacion)
        {
            var result = (from p in entities.CriteriosEvaluaciones where p.Id == criterioevaluacion.Id select p).First();

            result.Presencia = criterioevaluacion.Presencia;
            result.DominioTema = criterioevaluacion.DominioTema;

            entities.SaveChanges();
        }

        public void Delete(int id)
        {
            var result = (from p in entities.CriteriosEvaluaciones where p.Id == id select p).First();
            entities.CriteriosEvaluaciones.Remove(result);
            entities.SaveChanges();
        }

        public List<CriteriosEvaluacion> ByQueryAll(string query)
        {
            var dbQuery = (from p in entities.CriteriosEvaluaciones.Include("Puesto").Include("Postulante") select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.Personalidad.Contains(query));

            return dbQuery.ToList();
        }
    }
}
