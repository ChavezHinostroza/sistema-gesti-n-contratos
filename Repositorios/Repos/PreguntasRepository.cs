﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Imporamos
using Repository;
using Intefaces;
using Modelos;
using System.Data;


namespace Repositorios.Repos
{
  public  class PreguntasRepository: IPreguntas
    {
        ContratosContext entities;
        public PreguntasRepository(ContratosContext entities)
        {
            this.entities = entities;
        }
        public List<Preguntas> All()
        {
            var result = from p in entities.Preguntas select p;
            return result.ToList();
        }

        public void Store(Preguntas preguntas)
        {
            entities.Preguntas.Add(preguntas);
            entities.SaveChanges();
        }

        public Preguntas Find(int Id)
        {
            var result = from p in entities.Preguntas.Include("Puesto") where p.Id == Id select p;
            return result.FirstOrDefault();
        }

        public void Update(Preguntas preguntas)
        {
            var result = (from p in entities.Preguntas.Include("Puesto") where p.Id == preguntas.Id select p).First();

            result.Pregunta = preguntas.Pregunta;
            result.idPuesto = preguntas.idPuesto;
            
            entities.SaveChanges();
        }

        public void Delete(int Id)
        {
            var result = (from p in entities.Preguntas where p.Id == Id select p).First();
            entities.Preguntas.Remove(result);
            entities.SaveChanges();
        }

        public List<Preguntas> ByQueryAll(string query)
        {
            var dbQuery = (from p in entities.Preguntas.Include("Puesto") select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.Pregunta.Contains(query));

            return dbQuery.ToList();
        }

        public Preguntas GenerarPregAleatorias(int Id, String puesto)
        {

            var result = from p in entities.Preguntas.Include("Puesto") where p.Id == Id && p.Puesto.Nombre == puesto select p;
            return result.FirstOrDefault();
        }
    }
}
